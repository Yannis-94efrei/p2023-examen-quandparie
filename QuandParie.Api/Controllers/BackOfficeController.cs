﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuandParie.Api.Contracts;
using QuandParie.Core.Application;
using QuandParie.Core.ReadOnlyInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuandParie.Api.Controllers
{
    [ApiController]
    public class BackOfficeController : ControllerBase
    {
        private readonly BackOfficeAdministration application;

        public BackOfficeController(BackOfficeAdministration application)
        {
            this.application = application;
        }

        [HttpPost("/odds")]
        public Task<IReadOnlyOdds> CreateOdds(OddsCreation odds)
            => application.CreateOdds(odds.Match, odds.Condition, odds.Value);

        [HttpGet("/odds")]
        public Task<IReadOnlyList<IReadOnlyOdds>> GetOddsForMatch([Required] string match)
            => application.GetOddsForMatch(match);

        [HttpGet("/odds/{id}")]
        public async Task<ActionResult<IReadOnlyOdds>> GetOdds(Guid id)
        {
            IReadOnlyOdds result = await application.GetOdds(id);
            return result != null 
                ? new ActionResult<IReadOnlyOdds>(result) 
                : NotFound();
        }

        [HttpPut("/odds/{id}")]
        public async Task<ActionResult<IReadOnlyOdds>> PutOdds(Guid id, OddsChange change)
        {
            if (change.Id != id)
                return BadRequest($"The id {id} of the resource does not match the id {change.Id} of the body");

            var odds = await application.GetOdds(id);
            if (odds == null)
                return NotFound();
            
            if (odds.Outcome != change.Outcome)
            {
                if (odds.Outcome != null)
                    return BadRequest($"Cannot change the outcome of {id} from {odds.Outcome} to {change.Outcome}");
                
                await application.ReportResult(id, change.Outcome.Value);
            }

            if (odds.Value != change.Value)
            {
                if (odds.Outcome != null)
                    return BadRequest($"Cannot change the value of {id} because the outcome has already been reported");

                await application.ChangeOddsValue(id, change.Value);
            }

            return new ActionResult<IReadOnlyOdds>(odds);
        }
    }
}
